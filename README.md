# README

Prefira ler o README.md em um leitor online como https://dillinger.io/ 

CRUD CLIENTE SPRING BOOT

 * Desenvolva um projeto Java com Spring Boot (utilizando qualquer modulo que achar necessário), que possua uma api REST com CRUD de Cliente (id, nome, cpf, dataNascimento). O CRUD deve possuir uma api de GET, POST, DELETE, PATCH e PUT.
* A api de GET deve aceitar query strings para pesquisar os clientes por CPF e nome. Também é necessário que nessa api os clientes voltem paginados e que possua um campo por cliente com a idade calculado dele considerando a data de nascimento.
 * O banco de dados deve estar em uma imagem Docker ou em um sandbox SAAS (banco SQL).
* O projeto deve estar em um repositório no BitBucket e na raiz do projeto deve conter um Postman para apreciação da api


## ARQUITETURA
A aplicação foi desenvolvida com framework SpringBoot e estrutura de pacotes hexagonal não modular

### PACKAGES

* domain = O pacote CORE da aplicacao onde tem as REGRAS DE NEGOCIO(BO), as PORTAS(Ports) e tambem os Handlers principais que orquestram o fluxo.
* aplication = O pacaote onde teremos as implementações que adaptam nossas portas(ADAPTERS) eles são dividos em ENTRADA(INBOUND) e SAIDA(OUTBOUND), cada um possui individualidade e desacomplamento, como regra nada da camada de Inbound pode comunicar com a camada de Outbound e o inversso tambem, essas camadas precisam do Core para que ele orqueste o fluxo.
* commons = O pacote possui classes comuns que não impactam o negocio, que podem ser utilizadas em qualquer camada.

### SPRING BOOT   
    * Spring-boot-2.2.1.RELEASE
    * Spring-boot-starter-actuator
    * Spring-boot-starter-data-jpa
    * Spring-boot-starter-web
    
## INSTALAÇÃO E RUNNING

- 1º - BANCO DE DADOS

    ##### MYSQL 
    O banco de dados MySQL será ser executado em container docker. Os comandos para baixar a imagem e executar um container mysql são os seguintes:
    ```
     docker pull mysql:5.7
     docker run --name mysql-5.7 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=platformbuilder -e MYSQL_USER=sa -e MYSQL_PASSWORD=password -p 3306:3306 -d mysql:5.7 
    ```
   
   
   
- 2º - APLICAÇÃO

    ##### MAVEN
    Para fazer a instalação, devera usar Maven como ferramenta de build e java 11 ou superior para a compilação.
    Para fazer o deploy rode o clean install do maven e o java -jar do projeto
    ```sh
    $ mvn clean install
    $ java -jar cliente/target/cliente-1.0.0.jar
     ```
        
		
		
- 3º -  OPICIONAL 
       
    ##### H2 embarcado sem container
    Deixa um acesso não tão facil mas possivel com poucos ajustes no codigo para caso queira usar o banco de dados H2 embarcado na aplicação sem precisar instalar nenhum container. Não esta profissional poderia no caso usar Profiles para poder configurar essa perfil de comportamento mas como o foco era usar o banco em container deixei assim mesmo.Mas da para testar sem problemas, precisa que comente a configuração de banco mysql no application.yml que esta na pasta resource e em descomente a dependencia de H2 que esta no aruivo pom.xml na raiz do projeto e comente no mesmo pom.xml a dependencia MySql.
    
	```
    Comente essa configuracao de ponta ponta no \cliente-crud\src\main\resources\application.yml com # na frente de cada linha.
    -----CONFIGURACAO BANCO MYSQL-----    
    spring:
    datasource:
      username: sa
      password: password
      url: jdbc:mysql://0.0.0.0:3306/platformbuilder?useSSL=false&allowPublicKeyRetrieval=true&createIfNotExists=true
      driver-class-name: com.mysql.cj.jdbc.Driver
    jpa:
      properties:
        hibernate.dialect: org.hibernate.dialect.MySQL5Dialect
      hibernate.ddl-auto: update
      show-sql: true
     ----------------------------------  
    descomente a dependencia do H2
    	<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>
		
	comente a dependecia do MySQL
	<!-- 		<dependency> -->
    <!-- 			<groupId>mysql</groupId> -->
    <!-- 			<artifactId>mysql-connector-java</artifactId> -->
    <!-- 			<scope>runtime</scope> -->
    <!-- 		</dependency> -->
  
    ```


## DOCUMENTAÇÃO
Voce poderá usar o swagger-ui da aplicação como documentação após inciala
<http://localhost:8080/v1/swagger-ui.html> 
ou abrir um dos arquivos de documentação (SWAGGER.json ou SWAGGER-OPENAPI.yml) que se encontram na pasta root do projeto.

## OBSERVAÇÕES

A aplicação faz varias validações , como CPF informado deve ser valido, não pode colocar data de nascimento depois do dia atual, e outras validações que podem ser acompanhadas quando o retorno for 400.




[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)




  


    


