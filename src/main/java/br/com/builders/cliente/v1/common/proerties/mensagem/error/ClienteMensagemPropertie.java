package br.com.builders.cliente.v1.common.proerties.mensagem.error;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteMensagemPropertie {

	private String naoEncontrado;
}
