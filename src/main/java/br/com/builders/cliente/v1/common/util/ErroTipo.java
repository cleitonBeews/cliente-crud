package br.com.builders.cliente.v1.common.util;

public class ErroTipo {

	public static final Integer CLIENTE = 400;
	public static final Integer INTERNO = 500;
}
