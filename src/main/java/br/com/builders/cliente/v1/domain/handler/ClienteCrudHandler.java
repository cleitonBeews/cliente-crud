package br.com.builders.cliente.v1.domain.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import br.com.builders.cliente.v1.domain.dto.QueryClienteCoreDTO;
import br.com.builders.cliente.v1.domain.exception.ClienteNaoEncontrado;
import br.com.builders.cliente.v1.domain.handler.bo.ClienteBO;
import br.com.builders.cliente.v1.domain.port.inbound.ClienteCrudInboundPort;
import br.com.builders.cliente.v1.domain.port.outbound.ClienteRepositoyOutboundPort;

@Service
public class ClienteCrudHandler implements ClienteCrudInboundPort {

	@Autowired
	private ClienteRepositoyOutboundPort clienteOutboundPort;
	
	@Autowired
	private ClienteBO clienteBO;
	
	@Override
	public Page<ClienteCoreDTO> ler(QueryClienteCoreDTO queryCliente, Pageable pageable) {
		return  clienteOutboundPort.paginar(queryCliente, pageable)
		 .map(clienteBO::calculaIdadeDoCliente);
	}

	@Override
	public void criar(ClienteCoreDTO cliente) {
		 clienteOutboundPort.salvar(cliente);
	}

	@Override
	public void atualizar(ClienteCoreDTO cliente) throws ClienteNaoEncontrado {
		clienteOutboundPort.atualizar(cliente);
		
	}

	@Override
	public void deletar(Long id) throws ClienteNaoEncontrado {
		clienteOutboundPort.apagarPorId(id);
		
	}

	@Override
	public ClienteCoreDTO lerPorId(Long id) throws ClienteNaoEncontrado {
		return clienteBO.calculaIdadeDoCliente(clienteOutboundPort.buscarPorId(id));
	}

	
}
