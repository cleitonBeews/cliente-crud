package br.com.builders.cliente.v1.application.outbound.database.util;

import org.mapstruct.Mapper;

import br.com.builders.cliente.v1.application.outbound.database.entity.ClienteEntity;
import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;

@Mapper
public interface MapperOutboundDatabase {

	public ClienteEntity from(ClienteCoreDTO coreDto);
	public ClienteCoreDTO from(ClienteEntity outboundDto);
}
