package br.com.builders.cliente.v1.application.outbound.database.adapter;

import static java.util.Optional.ofNullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.builders.cliente.v1.application.outbound.database.repository.ClienteRepository;
import br.com.builders.cliente.v1.application.outbound.database.specification.SpecificationClienteEntity;
import br.com.builders.cliente.v1.application.outbound.database.util.MapperOutboundDatabase;
import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import br.com.builders.cliente.v1.domain.dto.QueryClienteCoreDTO;
import br.com.builders.cliente.v1.domain.exception.ClienteNaoEncontrado;
import br.com.builders.cliente.v1.domain.port.outbound.ClienteRepositoyOutboundPort;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ClienteRepositoryOutboundPortDatabaseAdapter implements ClienteRepositoyOutboundPort {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private MapperOutboundDatabase mapper;

	@Override
	public Page<ClienteCoreDTO> paginar(QueryClienteCoreDTO queryCliente, Pageable pageable){

		//Usando specification para fazer a query do cliente com CPF e NOME
		
		val pageClienteEntity = clienteRepository.findAll(Specification.where(SpecificationClienteEntity
				.nome(queryCliente.getNome())
				.or(SpecificationClienteEntity.cpf(queryCliente.getCpf()))), pageable);
		log.info("busca efetuada com sucesso");
		return pageClienteEntity.map(mapper::from);

	}

	@Override
	public void salvar(ClienteCoreDTO cliente) {

		clienteRepository.save(mapper.from(cliente));
		log.info("cliente salvo com sucesso");

	}

	@Override
	public void atualizar(ClienteCoreDTO cliente) throws ClienteNaoEncontrado {

		val clienteParaAtualizacao = clienteRepository.findById(cliente.getId())
				.orElseThrow(ClienteNaoEncontrado::new);

		ofNullable(cliente.getCpf()).ifPresent(cpf -> clienteParaAtualizacao.setCpf(cpf));

		ofNullable(cliente.getDataNascimento())
				.ifPresent(dataNascimento -> clienteParaAtualizacao.setDataNascimento(dataNascimento));

		ofNullable(cliente.getNome()).ifPresent(nome -> clienteParaAtualizacao.setNome(nome));

		clienteRepository.save(clienteParaAtualizacao);
		log.info("cliente atualizado com sucesso");

	}

	@Override
	public void apagarPorId(Long id)throws ClienteNaoEncontrado {
		val clienteParaSerDeletado = clienteRepository.findById(id)
				.orElseThrow(ClienteNaoEncontrado::new);
		clienteRepository.delete(clienteParaSerDeletado);
		log.info("cliente deletado com sucesso");

	}

	@Override
	public ClienteCoreDTO buscarPorId(Long id)throws ClienteNaoEncontrado {
		val cliente = clienteRepository.findById(id)
			.orElseThrow(ClienteNaoEncontrado::new);
		log.info("cliente encontrado com sucesso");
		return mapper.from(cliente);
	}

}
