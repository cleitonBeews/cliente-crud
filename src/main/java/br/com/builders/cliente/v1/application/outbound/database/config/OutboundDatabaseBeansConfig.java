package br.com.builders.cliente.v1.application.outbound.database.config;

import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.builders.cliente.v1.application.outbound.database.util.MapperOutboundDatabase;

@Configuration
public class OutboundDatabaseBeansConfig {

	@Bean
	public MapperOutboundDatabase getMapperOutboundDatabase() {
		return Mappers.getMapper(MapperOutboundDatabase.class);
	}
	
}
