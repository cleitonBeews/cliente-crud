package br.com.builders.cliente.v1.domain.port.inbound;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import br.com.builders.cliente.v1.domain.dto.QueryClienteCoreDTO;
import br.com.builders.cliente.v1.domain.exception.ClienteNaoEncontrado;

public interface ClienteCrudInboundPort {
	
	/*
	 * CRUD OPERATIONS
	 */
	
	//--CREATE--
	public void criar(ClienteCoreDTO cliente);
	
	//--READ--
	public Page<ClienteCoreDTO> ler(QueryClienteCoreDTO queryCliente,Pageable pageable) throws ClienteNaoEncontrado;
	public ClienteCoreDTO lerPorId(Long id) throws ClienteNaoEncontrado;
	
	//--UPDATE--
	public void atualizar(ClienteCoreDTO cliente) throws ClienteNaoEncontrado;
	
	//--DELETE--
	public void deletar(Long id) throws ClienteNaoEncontrado;

}
