package br.com.builders.cliente.v1.application.inbound.rest.request.json;

import java.time.LocalDate;

import javax.validation.constraints.PastOrPresent;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Data;

@Data
public class PatchClienteRequestJson {

	@CPF
	private String cpf;
	
	private String nome;
	
	@PastOrPresent
	private LocalDate dataNascimento;
}
