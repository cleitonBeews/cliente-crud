package br.com.builders.cliente.v1.application.inbound.rest.util;

import org.mapstruct.Mapper;

import br.com.builders.cliente.v1.application.inbound.rest.request.json.NovoClienteRequestJson;
import br.com.builders.cliente.v1.application.inbound.rest.request.json.PatchClienteRequestJson;
import br.com.builders.cliente.v1.application.inbound.rest.request.json.PutClienteRequestJson;
import br.com.builders.cliente.v1.application.inbound.rest.request.query.ClienteRequestQuery;
import br.com.builders.cliente.v1.application.inbound.rest.response.json.ClienteResponseJson;
import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import br.com.builders.cliente.v1.domain.dto.QueryClienteCoreDTO;

@Mapper
public interface MapperInboundRest{

	public ClienteRequestQuery from( QueryClienteCoreDTO coreDTO);
	public QueryClienteCoreDTO from(ClienteRequestQuery  inboundDTO);
	public ClienteResponseJson from (ClienteCoreDTO coreDTO);
	public ClienteCoreDTO from (PutClienteRequestJson inboundDTO);
	public ClienteCoreDTO from (PatchClienteRequestJson inboundDTO);
	public ClienteCoreDTO from (NovoClienteRequestJson inboundDTO);

}
