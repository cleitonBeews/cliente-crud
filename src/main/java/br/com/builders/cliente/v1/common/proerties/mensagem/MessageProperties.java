package br.com.builders.cliente.v1.common.proerties.mensagem;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import br.com.builders.cliente.v1.common.proerties.mensagem.error.CoreMessageProperties;
import br.com.builders.cliente.v1.common.proerties.mensagem.error.ErrorMessageProperties;
import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
@ConfigurationProperties("message")
public class MessageProperties {

	private ErrorMessageProperties error;
	private CoreMessageProperties core;
	

	
	
}
