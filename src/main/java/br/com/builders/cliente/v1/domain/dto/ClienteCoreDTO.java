package br.com.builders.cliente.v1.domain.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class ClienteCoreDTO {

	private Long id;
	private String nome;
	private String cpf;
	private LocalDate dataNascimento;
	private Integer idade;
}
