package br.com.builders.cliente.v1.application.outbound.database.specification;

import org.springframework.data.jpa.domain.Specification;

import br.com.builders.cliente.v1.application.outbound.database.entity.ClienteEntity;

public class SpecificationClienteEntity {

	public static Specification<ClienteEntity> nome(String nome) {
		
			return (root, criteriaQuery, criteriaBuilder) ->
				criteriaBuilder.like(root.get("nome"), "%" + nome + "%");
	
	}

	public static Specification<ClienteEntity> cpf(String cpf) {
		
			return (root, criteriaQuery, criteriaBuilder) -> 
				criteriaBuilder.like(root.get("cpf"), "%" + cpf + "%");
	
	}

}
