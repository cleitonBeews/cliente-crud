package br.com.builders.cliente.v1.application.inbound.rest.exception.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.google.common.base.CaseFormat;

import br.com.builders.cliente.v1.common.exception.RuntimeExceptionCommon;
import br.com.builders.cliente.v1.common.exception.dto.ExceptionCampoDTO;
import br.com.builders.cliente.v1.common.exception.dto.ExceptionDTO;
import br.com.builders.cliente.v1.common.exception.dto.MensagemExceptionDTO;
import br.com.builders.cliente.v1.common.proerties.mensagem.MessageProperties;
import br.com.builders.cliente.v1.domain.exception.ClienteNaoEncontrado;
import lombok.val;
import lombok.extern.log4j.Log4j2;

@ControllerAdvice
@RestController
@Log4j2
public class ExeptionHandler {

	@Autowired
	MessageProperties messageProperties;

	
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionDTO> handleException(Exception ex, WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setTitulo("ERRO INTERNO");
		errorDetails.setDescricao(messageProperties.getError().getServerError());
		errorDetails.setMensagem(MensagemExceptionDTO
				.builder()
				.usuario(messageProperties.getError().getUserInfo())
				.desenvolvedor(ex.getLocalizedMessage())
				.build());
		log.debug("expetion handler {}", ex);
		log.info("expetion handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	
	@ExceptionHandler(BindException.class)
	public final ResponseEntity<ExceptionDTO> handleException(BindException ex, WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		
		
		errorDetails.setTitulo("VALIDACAO QUERY");
		errorDetails.setDescricao(messageProperties.getError().getBadRequest());
		errorDetails.setMensagem(MensagemExceptionDTO
				.builder()
				.usuario(messageProperties.getError().getUserInfo())
				.desenvolvedor(ex.getLocalizedMessage())
				.build());
		log.debug("expetion handler {}", ex);
		log.info("expetion handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public final ResponseEntity<ExceptionDTO> handleException(HttpMessageNotReadableException ex, WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setTitulo("JSON INVALIDO");
		errorDetails.setDescricao(messageProperties.getError().getBadRequest());
		errorDetails.setMensagem(MensagemExceptionDTO
				.builder()
				.usuario(messageProperties.getError().getUserInfo())
				.desenvolvedor(ex.getLocalizedMessage())
				.build());
		log.debug("expetion handler {}", ex);
		log.info("expetion handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(ServletException.class)
	public final ResponseEntity<ExceptionDTO> handleException(ServletException ex,
			WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setTitulo(HttpStatus.NOT_FOUND.toString());
		errorDetails.setMensagem(MensagemExceptionDTO.builder()
				.desenvolvedor(ex.getLocalizedMessage())
				.build());
		log.debug("ServletException handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	
	
	
	
	@ExceptionHandler(HttpMessageConversionException.class)
	public final ResponseEntity<ExceptionDTO> handleException(HttpMessageConversionException ex,
			WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setDescricao(messageProperties.getError().getBadRequest());
		errorDetails.setMensagem(MensagemExceptionDTO.builder()
				.usuario(messageProperties.getError().getUserInfo())
				.desenvolvedor(ex.getLocalizedMessage())
				.build());
		log.debug("ServletException handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public final ResponseEntity<ExceptionDTO> handleException(MethodArgumentTypeMismatchException ex, WebRequest request) {
		
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setTitulo("ARGUMENTOS INVALIDOS");
		errorDetails.setMensagem(MensagemExceptionDTO.builder()
				.desenvolvedor(	CaseFormat.UPPER_CAMEL.to(
						CaseFormat.UPPER_UNDERSCORE, ex.getClass().getSimpleName()))
				.usuario(messageProperties.getError().getUserInfo())
				.build());
		
		log.debug("RuntimeExceptionCommon handler {}",errorDetails, ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(RuntimeExceptionCommon.class)
	public final ResponseEntity<ExceptionDTO> handleException(RuntimeExceptionCommon ex, WebRequest request) {
		
		ExceptionDTO errorDetails = ex.getDto();
		val status = HttpStatus.valueOf(ex.getTipoErro());
		
		errorDetails.setDescricao(getDescricao(status));
		log.debug("RuntimeExceptionCommon handler {}",errorDetails, ex);
		return new ResponseEntity<>(errorDetails, status);
	}
	
	@ExceptionHandler(ClienteNaoEncontrado.class)
	public final ResponseEntity<ExceptionDTO> handleException(ClienteNaoEncontrado ex, WebRequest request) {
		
		val status = HttpStatus.valueOf(ex.getTipoErro());
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setDescricao(getDescricao(status));
		errorDetails.setTitulo("CLIENTE");
		errorDetails.setMensagem(MensagemExceptionDTO.builder()
				.desenvolvedor(messageProperties.getCore().getCliente().getNaoEncontrado())
				.usuario(messageProperties.getCore().getCliente().getNaoEncontrado())
				.build()
				);
		
		log.debug("RuntimeExceptionCommon handler {}",errorDetails, ex);
		return new ResponseEntity<>(errorDetails, status);
	}


	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<ExceptionDTO> handleException(MethodArgumentNotValidException ex,
			WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setTitulo("VALIDACAO");
		List<ExceptionCampoDTO> errors = ex.getBindingResult().getFieldErrors().stream()
				.map(x -> ExceptionCampoDTO.builder().campo(x.getField()).mensagem(x.getDefaultMessage()).build()

				).collect(Collectors.toList());

		errorDetails.setDescricao(messageProperties.getError().getBadRequest());
		errorDetails.setCampos(errors);
		errorDetails.setMensagem(MensagemExceptionDTO.builder()
				.desenvolvedor(	CaseFormat.UPPER_CAMEL.to(
						CaseFormat.UPPER_UNDERSCORE, MethodArgumentNotValidException.class.getSimpleName()))
				.usuario(messageProperties.getError().getUserInfo())
				.build());

		log.debug("MethodArgumentNotValidException handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	

	@ExceptionHandler(MissingServletRequestParameterException.class)
	public final ResponseEntity<ExceptionDTO> handleException(MissingServletRequestParameterException ex,
			WebRequest request) {
		ExceptionDTO errorDetails = new ExceptionDTO();
		errorDetails.setDescricao(messageProperties.getError().getBadRequest());

		String mensagemDev = "Parametro '" + ex.getParameterName() + "' do tipo '" + ex.getParameterType()
				+ "' é obrigatório";
		errorDetails.setMensagem(MensagemExceptionDTO.builder()
				.usuario(messageProperties.getError().getUserInfo())
				.desenvolvedor(mensagemDev).build());
		errorDetails.setTitulo("PARAMETRO INVALIDO");
		log.debug("MissingServletRequestParameterException handler {}", ex);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	private final String getDescricao(HttpStatus status) {
		
		if(status == HttpStatus.UNAUTHORIZED)
			return messageProperties.getError().getNaoAutorizado();
		
		if(status.is4xxClientError())
			return messageProperties.getError().getBadRequest();
		
		return  messageProperties.getError().getServerError();
		
		
	}

}
