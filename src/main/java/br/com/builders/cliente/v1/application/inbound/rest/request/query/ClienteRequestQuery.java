package br.com.builders.cliente.v1.application.inbound.rest.request.query;

import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class ClienteRequestQuery {

	private String nome;
	
	@Pattern(regexp = "^[0-9]")
	private String cpf;
}
