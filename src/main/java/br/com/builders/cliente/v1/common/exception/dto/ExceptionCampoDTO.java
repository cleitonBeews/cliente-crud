package br.com.builders.cliente.v1.common.exception.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExceptionCampoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String campo;
	private String mensagem;	
	
}
