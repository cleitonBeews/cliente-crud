package br.com.builders.cliente.v1.domain.port.outbound;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import br.com.builders.cliente.v1.domain.dto.QueryClienteCoreDTO;
import br.com.builders.cliente.v1.domain.exception.ClienteNaoEncontrado;

public interface ClienteRepositoyOutboundPort {
	
	public void salvar(ClienteCoreDTO cliente);
	public Page<ClienteCoreDTO> paginar(QueryClienteCoreDTO queryCliente,Pageable pageable);
	public ClienteCoreDTO buscarPorId(Long id) throws ClienteNaoEncontrado;
	public void atualizar(ClienteCoreDTO cliente) throws ClienteNaoEncontrado;
	public void apagarPorId(Long id) throws ClienteNaoEncontrado;
	
}
