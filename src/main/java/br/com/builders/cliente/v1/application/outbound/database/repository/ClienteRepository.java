package br.com.builders.cliente.v1.application.outbound.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.builders.cliente.v1.application.outbound.database.entity.ClienteEntity;

public interface ClienteRepository extends JpaRepository<ClienteEntity, Long>, JpaSpecificationExecutor<ClienteEntity> {

}
