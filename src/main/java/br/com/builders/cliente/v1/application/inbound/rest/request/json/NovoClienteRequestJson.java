package br.com.builders.cliente.v1.application.inbound.rest.request.json;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Data;

@Data
public class NovoClienteRequestJson {

	@NotBlank
	@CPF
	private String cpf;
	
	@NotBlank
	private String nome;
	
	@NotNull
	@PastOrPresent
	private LocalDate dataNascimento;
}
