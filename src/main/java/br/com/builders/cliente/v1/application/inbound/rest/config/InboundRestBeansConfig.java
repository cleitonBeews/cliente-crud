package br.com.builders.cliente.v1.application.inbound.rest.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.cliente.v1.application.inbound.rest.util.MapperInboundRest;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class InboundRestBeansConfig {

	
	@Bean
	public MapperInboundRest getMapperInboundRest() {
		return Mappers.getMapper(MapperInboundRest.class);
	}
	
	@Bean
	public Docket api() throws FileNotFoundException, IOException, XmlPullParserException {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
				.paths(PathSelectors.any()).build()
				.apiInfo(apiInfo())
				.useDefaultResponseMessages(false); 
	}
	
	
	private ApiInfo apiInfo() throws FileNotFoundException, IOException, XmlPullParserException {
		var reader = new MavenXpp3Reader();
		Model model = reader.read(new FileReader("pom.xml"));
		
	     return new ApiInfo(
	    		 model.getArtifactId(),
	       "Descrição dos endpoints da aplicação " + model.getArtifactId(),
	       model.getVersion(), 
	       "Uso exclusivo para", 
	       new Contact("", "https://platformbuilders.io/", "sistemas@platformbuilders@com.br"), 
	       "", "API license URL", Collections.emptyList());
	}
	
}
