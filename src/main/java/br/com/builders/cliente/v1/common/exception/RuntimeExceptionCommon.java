package br.com.builders.cliente.v1.common.exception;

import br.com.builders.cliente.v1.common.exception.dto.ExceptionDTO;
import lombok.Getter;

@Getter
public class RuntimeExceptionCommon extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final ExceptionDTO dto;
	private final int tipoErro;

	public RuntimeExceptionCommon(int tipoErro) {
		this.tipoErro = tipoErro;
		dto = null;
	}
	

}
