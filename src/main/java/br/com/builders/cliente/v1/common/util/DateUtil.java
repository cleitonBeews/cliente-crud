package br.com.builders.cliente.v1.common.util;

import java.time.LocalDate;
import java.time.Period;

public class DateUtil {

	public static Integer diferenciaDeAnosEntre(LocalDate dataInicio, LocalDate dataFim) {
		return Period.between(dataInicio, dataFim).getYears();
	}
	
}
