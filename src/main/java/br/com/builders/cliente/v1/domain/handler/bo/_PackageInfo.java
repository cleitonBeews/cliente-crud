package br.com.builders.cliente.v1.domain.handler.bo;

public class _PackageInfo {

	/*
	 * Pacote destinado aos objetos de negocio (BO), eles deveram fazer funções
	 * simples e precisar ser testaveis de formas unitarias
	 * Os BOs serão classes SINGLETONs, deve-se tomar cuidado pois não podem
	 * ter objetos fora do seu escopo e nem injeção de depencias em seus atributos.
	 * 
	 */
	
}
