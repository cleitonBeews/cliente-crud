package br.com.builders.cliente.v1.application.inbound.rest.response.json;

import java.time.LocalDate;

import lombok.Data;

@Data
public class ClienteResponseJson {

	private Long id;
	private String cpf;
	private String nome;
	private LocalDate dataNascimento;
	private Integer idade;
}
