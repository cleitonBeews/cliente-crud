package br.com.builders.cliente.v1.domain.handler.bo;

import static br.com.builders.cliente.v1.common.util.DateUtil.diferenciaDeAnosEntre;
import static java.time.LocalDate.now;

import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import lombok.val;

public class ClienteBO {
	//SINGLETON CLASS
	
	private static ClienteBO instace;
	
	public static ClienteBO getInstace() {
		if(instace == null) {
			instace = new ClienteBO();
		}
		return instace;
	}
	
	public ClienteCoreDTO calculaIdadeDoCliente(ClienteCoreDTO cliente) {

		val idade = diferenciaDeAnosEntre(cliente.getDataNascimento(), now());
		cliente.setIdade(idade);
		return cliente;
	}
}
