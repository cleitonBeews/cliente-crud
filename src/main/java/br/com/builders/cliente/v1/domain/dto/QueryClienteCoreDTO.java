package br.com.builders.cliente.v1.domain.dto;

import lombok.Data;

@Data
public class QueryClienteCoreDTO {

	private String nome;
	private String cpf;
}
