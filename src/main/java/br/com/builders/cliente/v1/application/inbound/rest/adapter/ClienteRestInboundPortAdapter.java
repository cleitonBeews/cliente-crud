package br.com.builders.cliente.v1.application.inbound.rest.adapter;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.cliente.v1.application.inbound.rest.request.json.NovoClienteRequestJson;
import br.com.builders.cliente.v1.application.inbound.rest.request.json.PatchClienteRequestJson;
import br.com.builders.cliente.v1.application.inbound.rest.request.json.PutClienteRequestJson;
import br.com.builders.cliente.v1.application.inbound.rest.request.query.ClienteRequestQuery;
import br.com.builders.cliente.v1.application.inbound.rest.response.json.ClienteResponseJson;
import br.com.builders.cliente.v1.application.inbound.rest.util.MapperInboundRest;
import br.com.builders.cliente.v1.domain.exception.ClienteNaoEncontrado;
import br.com.builders.cliente.v1.domain.port.inbound.ClienteCrudInboundPort;
import io.swagger.annotations.Api;

@Api
@RestController
@RequestMapping("/resource")
public class ClienteRestInboundPortAdapter {

	private static final String CLIENTE = "/cliente";
	private static final String ID = "id";
	private static final String CLIENTE_ID =  CLIENTE + "/{"+ ID +"}";
	
	@Autowired
	private ClienteCrudInboundPort clienteCrudInboundPort;
	
	@Autowired
	MapperInboundRest mapper;
	
	@PostMapping(CLIENTE)
	public ResponseEntity<Void> novo(@Valid @RequestBody NovoClienteRequestJson clienteJson){
		
		var cliente = mapper.from(clienteJson);
		clienteCrudInboundPort.criar(cliente);
		return ResponseEntity.accepted().build();
	}
	
	@GetMapping(CLIENTE + "s")
	public ResponseEntity<Page<ClienteResponseJson>> buscarPorCriterios(
			@Valid ClienteRequestQuery clienteQuery, Pageable paginavel) throws ClienteNaoEncontrado{
		
		var clientes = clienteCrudInboundPort.ler(mapper.from(clienteQuery), paginavel);
		return ResponseEntity.ok(clientes.map(mapper::from));
	}
	
	
	@GetMapping(CLIENTE_ID)
	public ResponseEntity<ClienteResponseJson> buscarPorId(@Valid @PathVariable(ID)Long id) throws ClienteNaoEncontrado{
		
		var cliente = clienteCrudInboundPort.lerPorId(id);
		return ResponseEntity.ok(mapper.from(cliente));
	}
	

	@PutMapping(CLIENTE_ID)
	public ResponseEntity<Void> atualizar(
			@Valid @RequestBody PutClienteRequestJson clienteJson,
			@Valid @PathVariable(ID)Long id) throws ClienteNaoEncontrado{
		
		var cliente = mapper.from(clienteJson);
		cliente.setId(id);
		clienteCrudInboundPort.atualizar(cliente);
		return ResponseEntity.ok().build();
	}
	

	
	@PatchMapping(CLIENTE_ID)
	public ResponseEntity<Void> atualizarParcialmente(
			@Valid @RequestBody PatchClienteRequestJson clienteJson,
			@Valid @PathVariable(ID)Long id) throws ClienteNaoEncontrado{
		
		var cliente = mapper.from(clienteJson);
		cliente.setId(id);
		clienteCrudInboundPort.atualizar(cliente);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping(CLIENTE_ID)
	public ResponseEntity<Void> deletar(@Valid @PathVariable(ID)Long id) throws ClienteNaoEncontrado{
		
		clienteCrudInboundPort.deletar(id);
		return ResponseEntity.ok().build();
	}
	

}
