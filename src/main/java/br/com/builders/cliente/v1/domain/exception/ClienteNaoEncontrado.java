package br.com.builders.cliente.v1.domain.exception;

import br.com.builders.cliente.v1.common.exception.ExceptionCommon;
import br.com.builders.cliente.v1.common.util.ErroTipo;

public class ClienteNaoEncontrado extends ExceptionCommon{

	private static final long serialVersionUID = 1L;

	
	public ClienteNaoEncontrado() {
	
		super(ErroTipo.CLIENTE);
		
	}

}
