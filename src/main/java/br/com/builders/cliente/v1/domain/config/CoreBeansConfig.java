package br.com.builders.cliente.v1.domain.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.builders.cliente.v1.domain.handler.bo.ClienteBO;

@Configuration
public class CoreBeansConfig {

	@Bean
	public ClienteBO getClienteBO() {
		return ClienteBO.getInstace();
	}
}
