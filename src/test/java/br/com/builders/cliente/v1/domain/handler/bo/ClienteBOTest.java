package br.com.builders.cliente.v1.domain.handler.bo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.boot.test.context.SpringBootTest;

import br.com.builders.cliente.v1.domain.dto.ClienteCoreDTO;
import br.com.builders.cliente.v1.domain.handler.bo.ClienteBO;
import lombok.val;

@SpringBootTest
public class ClienteBOTest {

	ClienteBO clienteBO = ClienteBO.getInstace();
	
	public void deveValidarClienteSemIdadeEClienteComIdadeComAnosDeAcordoComADataAtual() {
		ClienteCoreDTO clienteCoreDTO = new ClienteCoreDTO();
		val dataDeNascimento = LocalDate.of(1989, 3, 22);
		clienteCoreDTO.setDataNascimento(dataDeNascimento);
		val idade = Period.between(dataDeNascimento, LocalDate.now()).getYears();
		
		assertEquals(null, clienteCoreDTO.getIdade());
		
		clienteCoreDTO = clienteBO.calculaIdadeDoCliente(clienteCoreDTO);
		
		assertEquals(idade, clienteCoreDTO.getIdade());
	}
	
}
